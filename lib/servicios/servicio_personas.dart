import 'dart:convert';

import 'package:tp2_app_medica/modelos/api_response.dart';
import 'package:tp2_app_medica/modelos/persona.dart';
import 'package:http/http.dart' as http;

class ServicioPersona {

  static const String api = 'https://equipoyosh.com/stock-nutrinatalia';

  static const headers = {
    'Content-type': 'application/json'
  };

  Future<APIResponse<List<Persona>>> getListaPersonas(){
    return http.get(Uri.parse(api + '/persona'))
        .then((data) {
      if(data.statusCode == 200) {
        final Map<String, dynamic> map = json.decode(data.body);
        final jsonData = map["lista"];
        final personas = <Persona>[];
        for(var elemento in jsonData) {
          personas.add(Persona.listarPersonaJson(elemento));
        }
        personas.last.idPersona;
        return APIResponse<List<Persona>>(data: personas);
      }
      return APIResponse<List<Persona>>(error: true, errorMessage: 'Ocurrio un error');
    }).catchError((error) => APIResponse<List<Persona>>(error: true, errorMessage: error.toString()));
  }

  Future<APIResponse<List<String>>> getUsuariosSistema(){
    return http.get(Uri.parse(api + '/persona?' + json.encode('ejemplo={"soloUsuariosDelSistema":true}')))
        .then((data) {
      if(data.statusCode == 200) {
        final Map<String, dynamic> map = json.decode(data.body);
        List<dynamic> jsonData = map["lista"];
        final usuarios = <String>[];

        for(var elemento in jsonData) {
          usuarios.add(elemento['usuarioLogin'] ?? '');
          print(usuarios[0]);
        }
        return APIResponse<List<String>>(data: usuarios, errorMessage: '${data.statusCode}');
      } else {
        return APIResponse<List<String>>(error: true, errorMessage: 'Error 500');
      }
    }).catchError((error) => APIResponse<List<String>>(error: true, errorMessage: error.toString()));
  }

  Future<APIResponse<List<Persona>>> getMedicos(){
    return http.get(Uri.parse(api + '/persona?ejemplo={"soloUsuariosDelSistema":true}'))
        .then((data) {
      if(data.statusCode == 200) {
        final Map<String, dynamic> map = json.decode(data.body);
        List<dynamic> jsonData = map["lista"];
        final usuarios = <Persona>[];
        print("Los usuarios del sistema son: ");
        print(usuarios.length);
        for(var elemento in jsonData) {
          usuarios.add(Persona.listarPersonaJson(elemento));
        }
        return APIResponse<List<Persona>>(data: usuarios, errorMessage: '${data.statusCode}');
      } else {
        return APIResponse<List<Persona>>(error: true, errorMessage: 'Error 500');
      }
    }).catchError((error) => APIResponse<List<Persona>>(error: true, errorMessage: error.toString()));
  }

  Future<APIResponse<List<Persona>>> getListaPersonasBusqueda(String texto){

    /* Uri _myUri = new Uri(
        scheme: 'https',
        host: 'www.kindacode.com',
        path: 'example/search/query',
        queryParameters: _myMap);

    print(_myUri);
*/
    return http.get(Uri.parse(api + '/persona?like=S&ejemplo={"nombre":"${texto}"}'))
        .then((data) {
      if(data.statusCode == 200) {
        final Map<String, dynamic> map = json.decode(data.body);
        final jsonData = map["lista"];
        final personas = <Persona>[];
        for(var elemento in jsonData) {
          personas.add(Persona.listarPersonaJson(elemento));
        }
        return APIResponse<List<Persona>>(data: personas);
      }
      return APIResponse<List<Persona>>(error: true, errorMessage: 'Ocurrio un error');
    }).catchError((error) => APIResponse<List<Persona>>(error: true, errorMessage: error.toString()));
  }


  Future<APIResponse<Persona>> getPersona(int id){
    return http.get(Uri.parse(api + '/persona/' + id.toString()))
        .then((data) {
      if(data.statusCode == 200) {
        final jsonData = json.decode(data.body);
        return APIResponse<Persona>(data: Persona.personaJson(jsonData));
      }
      return APIResponse<Persona>(error: true, errorMessage: 'Ocurrio un error');
    }).catchError((error) => APIResponse<Persona>(error: true, errorMessage: error.toString()));
  }

  Future<APIResponse<Persona>> crearPersona(Persona persona){
    return http.post(api + '/persona',headers: headers, body: json.encode(persona.toJson()))
        .then((data) {
      if(data.statusCode == 201) {
        final jsonData = json.decode(data.body);
        return APIResponse<Persona>(data: Persona.personaJson(jsonData));
      }
      return APIResponse<Persona>(error: true, errorMessage: data.statusCode.toString());
    }).catchError((error) => APIResponse<Persona>(error: true, errorMessage: error.toString()));
  }

  Future<APIResponse<Persona>> editarPersona(Persona persona){
    return http.put(api + '/persona',headers: headers, body: json.encode(persona.toJsonUpdate()))
        .then((data) {
      if(data.statusCode == 204) {
        final jsonData = json.decode(data.body);
        return APIResponse<Persona>(data: Persona.personaJson(jsonData));
      }
      print(data.body);
      return APIResponse<Persona>(error: true, errorMessage: data.reasonPhrase.toString());
    }).catchError((error) => APIResponse<Persona>(error: true, errorMessage: error.toString()));
  }

  Future<APIResponse<bool>> eliminarPersona(int idPersona){
    return http.delete(api + '/persona/' + idPersona.toString(),headers: headers)
        .then((data) {
      if(data.statusCode == 200) {
        return APIResponse<bool>(data: true);
      }
      return APIResponse<bool>(error: true, errorMessage: data.reasonPhrase.toString());
    }).catchError((error) => APIResponse<bool>(error: true, errorMessage: error.toString()));
  }

}