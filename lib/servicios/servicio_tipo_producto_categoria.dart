import 'dart:convert';

import 'package:tp2_app_medica/modelos/api_response.dart';
import 'package:tp2_app_medica/modelos/ficha_clinica.dart';
import 'package:http/http.dart' as http;
import 'package:tp2_app_medica/modelos/tipo_producto_categoria.dart';

class ServicioTipoProductoCategoria {

  static const String api = 'https://equipoyosh.com/stock-nutrinatalia';

  static const headers = {
    'Content-type': 'application/json'
  };

  Future<APIResponse<List<TipoProducto>>> getListaTipoProducto(){
    return http.get(Uri.parse(api + '/tipoProducto'))
        .then((data) {
          print(data.statusCode);
      if(data.statusCode == 200) {
        final Map<String, dynamic> map = json.decode(data.body);
        final jsonData = map["lista"];
        final tipoProductos = <TipoProducto>[];
        for(var elemento in jsonData) {
          tipoProductos.add(TipoProducto.tipoProductoJson(elemento));
        }
        return APIResponse<List<TipoProducto>>(data: tipoProductos);
      }
      return APIResponse<List<TipoProducto>>(error: true, errorMessage: 'Ocurrio un error');
    }).catchError((error) => APIResponse<List<TipoProducto>>(error: true, errorMessage: error.toString()));
  }


  Future<APIResponse<TipoProducto>> getTipoProducto(int id){
    return http.get(Uri.parse(api + '/tipoProducto/' + id.toString()))
        .then((data) {
      if(data.statusCode == 200) {
        final jsonData = json.decode(data.body);
        return APIResponse<TipoProducto>(data: TipoProducto.tipoProductoJson(jsonData));
      }
      return APIResponse<TipoProducto>(error: true, errorMessage: 'Ocurrio un error');
    }).catchError((error) => APIResponse<TipoProducto>(error: true, errorMessage: error.toString()));
  }

  Future<APIResponse<List<Categoria>>> getListaCategoria(){
    return http.get(Uri.parse(api + '/categoria'))
        .then((data) {
      if(data.statusCode == 200) {
        final Map<String, dynamic> map = json.decode(data.body);
        final jsonData = map["lista"];
        final categorias = <Categoria>[];
        for(var elemento in jsonData) {
          categorias.add(Categoria.categoriaJson(elemento));
        }
        return APIResponse<List<Categoria>>(data: categorias);
      }
      return APIResponse<List<Categoria>>(error: true, errorMessage: 'Ocurrio un error');
    }).catchError((error) => APIResponse<List<Categoria>>(error: true, errorMessage: error.toString()));
  }


  Future<APIResponse<Categoria>> getCategoria(int id){
    return http.get(Uri.parse(api + '/categoria/' + id.toString()))
        .then((data) {
      if(data.statusCode == 200) {
        final jsonData = json.decode(data.body);
        return APIResponse<Categoria>(data: Categoria.categoriaJson(jsonData));
      }
      return APIResponse<Categoria>(error: true, errorMessage: 'Ocurrio un error');
    }).catchError((error) => APIResponse<Categoria>(error: true, errorMessage: error.toString()));
  }

}