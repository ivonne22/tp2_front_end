import 'package:flutter/cupertino.dart';

class Persona
{
  int idPersona;
  String nombre;
  String apellido;
  String telefono;
  String email;
  String ruc;
  String cedula;
  String tipoPersona;
  String fechaNacimiento;

  Persona({this.idPersona, this.nombre, this.apellido, this.telefono, this.email, this.ruc, this.cedula, this.tipoPersona, this.fechaNacimiento});
  Persona.empty();

  factory Persona.listarPersonaJson(Map<String, dynamic> elemento) {
    return Persona(
        idPersona: elemento['idPersona'],
        nombre: (elemento['nombre'] ?? ''),
        apellido: (elemento['apellido'] ?? ''),
        telefono: (elemento['telefono'] ?? ''),
        email: (elemento['email'] ?? ''),
        ruc: (elemento['ruc'] ?? ''),
        cedula: (elemento['cedula'] ?? ''),
        tipoPersona: (elemento['tipoPersona'] ?? ''),
        fechaNacimiento: (elemento['fechaNacimiento'] ?? '').toString()
    );
  }

  factory Persona.personaJson(Map<String, dynamic> elemento) {
    if(elemento!=null) {
      return Persona(
          idPersona: elemento['idPersona'],
          nombre: (elemento['nombre'] ?? ''),
          apellido: (elemento['apellido'] ?? ''),
          telefono: (elemento['telefono'] ?? ''),
          email: (elemento['email'] ?? ''),
          ruc: (elemento['ruc'] ?? ''),
          cedula: (elemento['cedula'] ?? ''),
          tipoPersona: (elemento['tipoPersona'] ?? ''),
          fechaNacimiento: (elemento['fechaNacimiento'] ?? '').toString()
      );
    }else{
      return Persona.empty();
    }
  }

  Map<String, dynamic> toJson() {
    return {
      "nombre": nombre,
      "apellido": apellido,
      "telefono": telefono,
      "email": email,
      "ruc": ruc,
      "cedula": cedula,
      "tipoPersona": tipoPersona,
      "fechaNacimiento": fechaNacimiento
    };
  }

  Map<String, dynamic> toJsonEjemplo() {
    return {
      "nombre": nombre,
    };
  }

  Map<String, dynamic> toJsonUpdate() {
    return {
      "idPersona": idPersona,
      "nombre": nombre,
      "apellido": apellido,
      "telefono": telefono,
      "email": email,
      "ruc": ruc,
      "cedula": cedula,
      "tipoPersona": tipoPersona,
      "fechaNacimiento": fechaNacimiento
    };
  }

}