
class TipoProducto{
    int idTipoProducto;
    String descripcion;
    Categoria categoria;

    TipoProducto({this.idTipoProducto, this.descripcion, this.categoria});

    factory TipoProducto.tipoProductoJson(Map<String, dynamic> elemento) {
        return TipoProducto(
            idTipoProducto: (elemento['idTipoProducto']),
            descripcion: (elemento['descripcion'] ?? ''),
            categoria: Categoria.categoriaJson(elemento['idCategoria']),
        );
    }

    Map<String, dynamic> toJson() {
        return {
            "descripcion": descripcion,
            "categoria": categoria,
        };
    }

}

class Categoria{
    int idCategoria;
    String descripcion;

    Categoria({this.idCategoria, this.descripcion});

    factory Categoria.categoriaJson(Map<String, dynamic> elemento) {
        return Categoria(
            descripcion: (elemento['descripcion'] ?? ''),
        );
    }

    Map<String, dynamic> toJson() {
        return {
            "descripcion": descripcion,
        };
    }


}

