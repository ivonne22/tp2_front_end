import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:tp2_app_medica/inicio/inicio.dart';
import 'package:tp2_app_medica/modelos/api_response.dart';
import 'package:tp2_app_medica/modelos/persona.dart';
import 'package:tp2_app_medica/servicios/servicio_personas.dart';
//import 'package:tp2_app_medica/Vistas/products_page.dart'; DEFINIR AQUI EL PATH DE LA VISTA PARA LA HOMEPAGE

class LoginPage extends StatefulWidget {
  static String id= 'login_page';

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with InputValidationMixin {
  final formGlobalKey = GlobalKey < FormState > ();

  ServicioPersona get servicio => GetIt.I<ServicioPersona>();
  APIResponse<List<String>> _apiResponse;
  //APIResponse<List<Persona>> _listaPersonas;
  bool _estaCargando = false;
  var userLogin;

  String formatDateTime(DateTime dateTime){
    return '${dateTime.day}/${dateTime.month}/${dateTime.year}';
  }

  @override
  void initState() {
    _fetchPersonas();
    super.initState();
  }

  _fetchPersonas() async {
    setState(() {
      _estaCargando = true;
    });

    _apiResponse = await servicio.getUsuariosSistema();
    //_listaPersonas = await servicio.getListaUsuarios();

    setState(() {
      _estaCargando = false;
    });
  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          title: Text('Iniciar sesión'),
        ),
        body:
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Form(
            key: formGlobalKey,
            child: Column(
             mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: 15,),
                Flexible(
                  child: Image.asset('images/hospital.png', height: 300.0),
                ),
                SizedBox(height: 15,),
                _userTextField(),
                SizedBox(height: 15,),
                _passwordTextField(),
                SizedBox(height: 20,),
                _bottonLogin(),
              ],
            ),
          ),
        ));
  }
  Widget _userTextField() {
    return StreamBuilder(
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: TextFormField(
              validator: (usuario) {

                var val=isUsuarioValido(usuario, _apiResponse.data) ;
                print(val);
                if ( val != null && val==true ) {

                  return null;
                } else
                  return 'Usuario inválido';
                },
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                icon: Icon(Icons.email),
                hintText: 'Login del usuario',
                labelText: 'Usuario',
              ),
              onChanged: (value) {},
              onSaved: (String value) {
                userLogin = value;
              },
            ),
          );
        }
    );
  }

  Widget _passwordTextField() {
    return StreamBuilder(
        builder: (BuildContext context, AsyncSnapshot snapshot){
          return Container(

            padding: EdgeInsets.symmetric(horizontal: 20),
            child: TextFormField(
              validator: (password) {
                if (isPasswordValid(password)) return null;
                else
                  return 'Contraseña inválida. Minimo 6 caracteres';
              },
              keyboardType: TextInputType.emailAddress,
              obscureText: true,
              decoration: InputDecoration(
                icon: Icon(Icons.lock),
                hintText: 'Contraseña',
                labelText: 'Contraseña',
              ),
              onChanged: (value){},
            ),
          );
        }
    );
  }

  Widget _bottonLogin() {
    return StreamBuilder(
        builder: (BuildContext context, AsyncSnapshot snapshot){
          return ElevatedButton(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 65, vertical: 20),
                child: Text('Iniciar sesión',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold)
                ),
              ),
              onPressed:(){
                if (formGlobalKey.currentState.validate()) {
                  formGlobalKey.currentState.save();
                  // use the email provided here
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => PantallaInicio(usuario: this.userLogin)));
                }
              }


          );
        }
    );
  }
}

mixin InputValidationMixin {

  bool isPasswordValid(String password) => password.length >= 6;

  bool isUsuarioValido(String usuario, List<String> lista)  {
    print(lista);
    print("lista");
    if(usuario.length==0) return false;
    var contain = lista.where((element) => element == usuario);

    if(contain.isEmpty) {
      return false;
    } else {
      return true;
    }
  }
}

