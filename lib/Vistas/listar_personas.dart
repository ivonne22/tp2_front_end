// ignore_for_file: unused_field

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:tp2_app_medica/Vistas/crear_modificar_persona.dart';
import 'package:tp2_app_medica/Vistas/eliminar_personas.dart';
import 'package:tp2_app_medica/modelos/api_response.dart';
import 'package:tp2_app_medica/modelos/persona.dart';
import 'package:tp2_app_medica/servicios/servicio_personas.dart';

class ListaPersonas extends StatefulWidget {

  @override
  State<ListaPersonas> createState() => _ListaPersonasState();
}

class _ListaPersonasState extends State<ListaPersonas> {
  ServicioPersona get servicio => GetIt.I<ServicioPersona>();
  APIResponse<List<Persona>> _apiResponse;
  APIResponse<List<Persona>> _apiResponseBusqueda;
  List<Persona> _personas;
  bool _estaCargando = false;

  String formatDateTime(DateTime dateTime){
    return '${dateTime.day}/${dateTime.month}/${dateTime.year}';
  }

  @override
  void initState() {
    _fetchPersonas();
    super.initState();
  }

  _fetchPersonas() async {
    setState(() {
      _estaCargando = true;
    });

    _apiResponse = await servicio.getListaPersonas();
    _personas = _apiResponse.data;

    setState(() {
      _estaCargando = false;
    });
  }

  _fetchPersonasBusqueda(String texto) async {
    /*setState(() {
      _estaCargando = true;
    }); */

    _apiResponseBusqueda = await servicio.getListaPersonasBusqueda(texto);
    _personas = _apiResponseBusqueda.data;

    setState(() {
      _estaCargando = false;
    });
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Lista de Personas')),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (builder)=> CrearModificarPersona()))
              .then((_) {
            _fetchPersonas();
          });
        },
        child: Icon(Icons.add),
      ), // floatingActionButton
      body: Builder(
          builder: (_) {
            if(_estaCargando) {
              return Center(child: CircularProgressIndicator());
            }

            if(_apiResponse.error) {
              return Center(child: Text(_apiResponse.errorMessage));
            }

            return ListView.separated(
              itemBuilder: (context, index) {
                return index == 0 ? _barraBusqueda() : _listaElementos(index-1);
              },
              separatorBuilder: (context, index) => Divider(height: 1, color: Colors.green),
              itemCount: _personas.length+1,
            );
          }
      ),
    );
  }

  _barraBusqueda(){
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: TextField(
            decoration: const InputDecoration(
                hintText: 'Buscar...'
            ),
            onChanged: (texto){
              if(texto != ''){
                texto = texto.toLowerCase();
                _fetchPersonasBusqueda(texto);
              } else {
                _fetchPersonas();
              }
            })
    );
  }

  _listaElementos(index){
    return Dismissible(
      key: ValueKey(_personas[index].idPersona),
      direction: DismissDirection.startToEnd,
      onDismissed: (direction) {

      },
      confirmDismiss: (direction) async {
        final resultado = await showDialog(
            context: context,
            builder: (_) => EliminarPersona()
        );

        if(resultado) {
          final eliminarResultado = await servicio.eliminarPersona(_personas[index].idPersona);

          var mensaje;
          if (eliminarResultado != null && eliminarResultado.data == true) {
            mensaje = 'La persona ha sido eliminada';
          } else {
            mensaje = eliminarResultado?.errorMessage ?? 'Ocurrió un error';
          }

          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(mensaje),
            duration: new Duration(milliseconds: 1000),
          ));

          return eliminarResultado?.data ?? false;

        }


        return resultado;
      },
      background: Container(
        color: Colors.red,
        padding: const EdgeInsets.only(left: 16),
        child: const Align(child: Icon(Icons.delete, color: Colors.white), alignment: Alignment.centerLeft),
      ),
      child: ListTile(
        title: Text(
          _personas[index].nombre + " " + _personas[index].apellido,
          style: TextStyle(color: Theme.of(context).primaryColor),
        ),
        subtitle: Text('Fecha de nacimiento ${
            _personas[index].fechaNacimiento}'),
        onTap: () {
          Navigator.of(context)
              .push(MaterialPageRoute(
              builder: (_) => CrearModificarPersona(idPersona: _personas[index].idPersona)))
              .then((data) {
            _fetchPersonas();
          });
        },
      ),
    );

  }
}
