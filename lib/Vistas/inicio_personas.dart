import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tp2_app_medica/Vistas/listar_personas.dart';
import 'package:tp2_app_medica/inicio/componentes/widget_drawer.dart';

class InicioPersonas extends StatelessWidget {
  static MediaQueryData _mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double defaultSize;
  static Orientation orientation;

  String usuario;
  InicioPersonas({this.usuario});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Pacientes'),
        ),
        drawer: MenuLateral(),
        body: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Flexible(
                    child: Image.asset('images/logo.png', height: 350),
                  ),
                  SizedBox(height: 20,),
                  _ListarPacientes(),
                ]
            )
        ),
      ),
    );
  }

  Widget _ListarPacientes() {
    return StreamBuilder(
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return ElevatedButton(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 89, vertical: 20),
                child: Text('Listar pacientes',
                    style: TextStyle(
                        fontSize: 20)),
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ListaPersonas()));
              });
        }
    );
  }
}