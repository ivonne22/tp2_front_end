// ignore_for_file: unused_field

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:tp2_app_medica/Vistas/ficha_clinica/crear_modificar_ficha_clinica.dart';
import 'package:tp2_app_medica/Vistas/ficha_clinica/eliminar_ficha_clinica.dart';
import 'package:tp2_app_medica/modelos/api_response.dart';
import 'package:tp2_app_medica/modelos/ficha_clinica.dart';
import 'package:tp2_app_medica/servicios/servicio_ficha_clinica.dart';

class ListaFichasClinicas extends StatefulWidget {
  String usuario;
  ListaFichasClinicas({this.usuario});
  @override
  State<ListaFichasClinicas> createState() => _ListaFichasClinicasState(usuario: usuario);
}

class _ListaFichasClinicasState extends State<ListaFichasClinicas> {
  String usuario;
  _ListaFichasClinicasState({this.usuario});
  ServicioFichaClinica get servicio => GetIt.I<ServicioFichaClinica>();
  APIResponse<List<FichaClinica>> _apiResponse;
  bool _estaCargando = false;

  String formatDateTime(DateTime dateTime){
    return '${dateTime.day}/${dateTime.month}/${dateTime.year}';
  }

  @override
  void initState() {
    _fetchFichasClinicas();
    super.initState();
  }

  _fetchFichasClinicas() async {
    setState(() {
      _estaCargando = true;
    });

    _apiResponse = await servicio.getListaFichasClinicas();
    print(_apiResponse.data.toString());

    setState(() {
      _estaCargando = false;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Lista de FichasClinicas')),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (builder)=> CrearModificarFichaClinica(fichaClinica: null, usuario:this.usuario)))
              .then((_) {
            _fetchFichasClinicas();
          });
        },
        child: Icon(Icons.add),
      ), // floatingActionButton
      body: Builder(
        builder: (_) {
          if(_estaCargando) {
            return Center(child: CircularProgressIndicator());
          }

          if(_apiResponse.error) {
            return Center(child: Text(_apiResponse.errorMessage));
          }

          return ListView.separated(
              itemBuilder: (context, index) {
                return Dismissible(
                  key: ValueKey(_apiResponse.data[index].idFichaClinica),
                  direction: DismissDirection.startToEnd,
                  onDismissed: (direction) {

                  },
                  confirmDismiss: (direction) async {
                    final resultado = await showDialog(
                        context: context,
                        builder: (_) => EliminarFichaClinica()
                    );

                    if(resultado) {
                      final eliminarResultado = await servicio.eliminarFichaClinica(_apiResponse.data[index].idFichaClinica);

                      var mensaje;
                      if (eliminarResultado != null && eliminarResultado.data == true) {
                        mensaje = 'La ficha clinica ha sido eliminada';
                      } else {
                        mensaje = eliminarResultado?.errorMessage ?? 'Ocurrió un error';
                      }

                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(mensaje),
                        duration: new Duration(milliseconds: 1000),
                      ));

                      print(eliminarResultado.data);

                      return eliminarResultado?.data ?? false;

                    }


                    return resultado;
                  },
                  background: Container(
                    color: Colors.red,
                    padding: const EdgeInsets.only(left: 16),
                    child: const Align(child: Icon(Icons.delete, color: Colors.white), alignment: Alignment.centerLeft),
                  ),
                  child: ListTile(
                    title: Text(
                      _apiResponse.data[index].cliente.nombre + " " + _apiResponse.data[index].cliente.apellido,
                      style: TextStyle(color: Theme.of(context).primaryColor),
                    ),
                    subtitle: Text('Motivo de consulta: ${
                        _apiResponse.data[index].motivoConsulta} '
                        '\nDiagnostico: ${_apiResponse.data[index].diagnostico}'
                        '\nDoctor: ${_apiResponse.data[index].empleado.nombre} ${_apiResponse.data[index].empleado.apellido}'),
                    onTap: () {
                      Navigator.of(context)
                      .push(MaterialPageRoute(
                          builder: (_) => CrearModificarFichaClinica(fichaClinica: _apiResponse.data[index], usuario: this.usuario)))
                          .then((data) {
                            _fetchFichasClinicas();
                      });
                    },
                  ),
                );
              },
              separatorBuilder: (context, index) => Divider(height: 1, color: Colors.green),
              itemCount: _apiResponse.data.length,
            );
        }
      ),
    );
  }
}
