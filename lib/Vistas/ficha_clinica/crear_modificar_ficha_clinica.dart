import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:tp2_app_medica/modelos/api_response.dart';
import 'package:tp2_app_medica/modelos/ficha_clinica.dart';
import 'package:tp2_app_medica/modelos/persona.dart';
import 'package:tp2_app_medica/modelos/tipo_producto_categoria.dart';
import 'package:tp2_app_medica/servicios/servicio_ficha_clinica.dart';
import 'package:tp2_app_medica/servicios/servicio_personas.dart';
import 'package:tp2_app_medica/servicios/servicio_tipo_producto_categoria.dart';

class CrearModificarFichaClinica extends StatefulWidget {
  String usuario;

  final FichaClinica fichaClinica;
  CrearModificarFichaClinica({this.fichaClinica,this.usuario});

  @override
  _CrearModificarFichaClinicaState createState() => _CrearModificarFichaClinicaState();
}


class _CrearModificarFichaClinicaState extends State<CrearModificarFichaClinica> {
  bool get estaEditando => widget.fichaClinica != null;

  bool _estaCargando = false;
  APIResponse<List<Persona>> _apiResponseCliente;
  APIResponse<List<Persona>> _apiResponseEmpleado;
  APIResponse<List<TipoProducto>> _apiResponseTipoProducto;
  APIResponse<Persona> _apiResponseUnCliente;
  APIResponse<Persona> _apiResponseUnEmpleado;
  APIResponse<TipoProducto> _apiResponseUnTipoProducto;
  List<Persona> _listaClientes;
  List<Persona> _listaEmpleados;
  List<TipoProducto> _listaTipoProducto;
  Persona _empleado;
  Persona _cliente;
  TipoProducto _tipoProducto;
  String _clienteElegido;
  String _empleadoElegido;
  String _tipoProductoElegido;
  FichaClinica fichaClinica;
  String mensajeError;


  ServicioPersona get servicioPersona => GetIt.I<ServicioPersona>();
  ServicioFichaClinica get servicioFichaClinica => GetIt.I<ServicioFichaClinica>();
  ServicioTipoProductoCategoria get servicioTipoProducto => GetIt.I<ServicioTipoProductoCategoria>();

  TextEditingController _controladorMotivoConsulta = TextEditingController();
  TextEditingController _controladorDiagnostico = TextEditingController();
  TextEditingController _controladorObservacion = TextEditingController();


  @override
  void initState() {
    super.initState();
    _fetchClientes();
    _fetchEmpleados();
    _fetchProductos();

    if (estaEditando){
      /*
      setState(() {
        _estaCargando = true;
      });

      servicioFichaClinica.getFichaClinica(widget.idFichaClinica)
          .then((response) {
        setState(() {
          _estaCargando = false;
        });

        fichaClinica = response.data;
        _controladorMotivoConsulta.text = fichaClinica.motivoConsulta;
        _controladorDiagnostico.text = fichaClinica.diagnostico;
        _controladorObservacion.text = fichaClinica.observacion;

        if (response.error) {
          mensajeError = response.errorMessage ?? 'Ocurrió un error';
        }

    });*/
      _controladorMotivoConsulta.text = widget.fichaClinica.motivoConsulta;
      _controladorDiagnostico.text = widget.fichaClinica.diagnostico;
      _controladorObservacion.text=widget.fichaClinica.observacion;
    }

  }


  _fetchClientes() async {
    setState(() {
      _estaCargando = true;
    });

    _apiResponseCliente = await servicioPersona.getListaPersonas();
    _listaClientes = _apiResponseCliente.data;

    setState(() {
      _estaCargando = false;
    });
  }
  _fetchProductos() async {
    setState(() {
      _estaCargando = true;
    });
    print("antes de llamar api");
    _apiResponseTipoProducto = await servicioTipoProducto.getListaTipoProducto();
    _listaTipoProducto = _apiResponseTipoProducto.data;

    setState(() {
      _estaCargando = false;
    });
  }
  _fetchEmpleados() async {
    setState(() {
      _estaCargando = true;
    });
    _apiResponseEmpleado = await servicioPersona.getMedicos();
    _listaEmpleados = _apiResponseEmpleado.data;

    setState(() {
      _estaCargando = false;
    });
  }

  getServicios(empleado, cliente, tipoProducto) async {
    _apiResponseUnEmpleado = await this.servicioPersona.getPersona(empleado);
    _empleado = _apiResponseUnEmpleado.data;

    _apiResponseUnCliente = await this.servicioPersona.getPersona(cliente);
    _cliente = _apiResponseUnCliente.data;



  }
  getEmpleado(empleado) async {
    _apiResponseUnEmpleado = await this.servicioPersona.getPersona(empleado);
    _empleado = _apiResponseUnEmpleado.data;
    print(_empleado.nombre);
  }

  getCliente(cliente) async {
    _apiResponseUnCliente = await this.servicioPersona.getPersona(cliente);
    _cliente = _apiResponseUnCliente.data;
    print(_cliente.nombre);
  }
  getTipoProducto(tipoProducto) async {
    _apiResponseUnTipoProducto = await this.servicioTipoProducto.getTipoProducto(tipoProducto);
    _tipoProducto = _apiResponseUnTipoProducto.data;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(title: Text(estaEditando ? 'Editar Ficha' : 'Crear Ficha')),
        body: Padding(
        padding: const EdgeInsets.all(12.0),
    child: _estaCargando
    ? Center(child: CircularProgressIndicator())
        : Column(
    children: <Widget>[


  TextField(
    controller: _controladorMotivoConsulta,
    decoration: const InputDecoration(
        hintText: 'Motivo de consulta'
    ),
    enabled: estaEditando ? false : true,
  ),


  Container(height: 8),


  TextField(
    controller: _controladorDiagnostico,
    decoration: const InputDecoration(
        hintText: 'Diagnostico'
    ),
    enabled: estaEditando ? false : true,
  ),


  Container(height: 8),


      TextField(
        controller: _controladorObservacion,
        decoration: const InputDecoration(
            hintText: 'Observacion'
        ),
      ),


      Container(height: 8),



      ListTile(
        title: const Text('Cliente'),
        enabled: estaEditando ? false : true,
        trailing:  DropdownButton(
            items: _listaClientes.map((item) {
              return DropdownMenuItem(
                child: Text(item.nombre + " " + item.apellido),
                value: item.idPersona.toString(),
              );
            }).toList(),
            onChanged: (newVal) {
              setState(() {
                _clienteElegido = newVal;
                getCliente(int.parse(_clienteElegido));
              });
            },
            value: _clienteElegido
        ),
      ),

      Container(height: 8),

      ListTile(
        title: const Text('Medicos'),
        enabled: estaEditando ? false : true,
        trailing:  DropdownButton(
            items: _listaEmpleados.map((item) {
              return DropdownMenuItem(
                child: Text(item.nombre + " " + item.apellido),
                value: item.idPersona.toString(),
              );
            }).toList(),
            onChanged: (newVal) {
              setState(() {
                _empleadoElegido = newVal;
                getEmpleado(int.parse(_empleadoElegido));
              });
            },
            value: _empleadoElegido
        ),
      ),

      Container(height: 8),

      ListTile(
        title: const Text('Tipo Producto'),
        enabled: estaEditando ? false : true,
        trailing:  DropdownButton(
            items: _listaTipoProducto.map((item) {
              return DropdownMenuItem(
                child: Text(item.descripcion.toString()),
                value: item.idTipoProducto.toString(),
              );
            }).toList(),
            onChanged: (newVal) {
              setState(() {
                _tipoProductoElegido = newVal;
                getTipoProducto(int.parse(_tipoProductoElegido));
              });
            },
            value: _tipoProductoElegido
        ),
      ),


      SizedBox(
        width: double.infinity,
        height: 35,
        child:ElevatedButton( //Este en verdad es mi widget de boton que esta wrapeado en sizedbox pero solo para cambiar el tamaño del boton
          child: const Text('Guardar', style: TextStyle(color: Colors.white)),
          style: ElevatedButton.styleFrom(
            /* primary: Colors.redAccent, // fijar el color de la caja del boton
                 side: BorderSide(width: 3, color: Colors.brown), // configura los lados del boton
                  elevation: 3, // pone que tan elevado va a estar el boton
                  shape: RoundedRectangleBorder( // Para colocar el radio de boton
                    borderRadius: BorderRadius.circular(50)
                  ),
                  padding: EdgeInsets.all(5),
                  alignment: Alignment.bottomCenter, */
          ),
          onPressed: () async {
            if (estaEditando) {

              setState(() {
                _estaCargando = true;
              });



              final ficha = FichaClinica(
                idFichaClinica: widget.fichaClinica.idFichaClinica,
                observacion: _controladorObservacion.text,
              );

              final resultado = await servicioFichaClinica.editarFichaClinica(ficha);

              setState(() {
                _estaCargando = false;
              });

              const titulo = 'Actualizado';
              final texto = resultado.error ? (resultado.errorMessage ??
                  'Ocurrió un error') : 'La persona ha sido actualizada';


              showDialog(context: context,
                  builder: (_) =>
                      AlertDialog(
                        title: Text(titulo),
                        content: Text(texto),
                        actions: <Widget>[
                          TextButton(
                            child: const Text('Ok'),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          )
                        ],
                      )
              )
                  .then((data) {
                Navigator.of(context).pop();
              });
            } else {

              setState(() {
                _estaCargando = true;
              });

              print(_cliente.nombre);
              final ficha = FichaClinica(
                observacion: _controladorObservacion.text,
                motivoConsulta: _controladorMotivoConsulta.text,
                diagnostico: _controladorDiagnostico.text,
                empleado: _empleado,
                cliente: _cliente,
                tipoProducto: _tipoProducto,
              );
              final resultado = await servicioFichaClinica.crearFichaClinica(ficha);

              setState(() {
                _estaCargando = false;
              });

              const titulo = 'Creado';
              final texto = resultado.error ? (resultado.errorMessage ??
                  'Ocurrió un error') : 'La ficha ha sido creada';


              showDialog(context: context,
                  builder: (_) =>
                      AlertDialog(
                        title: Text(titulo),
                        content: Text(texto),
                        actions: <Widget>[
                          TextButton(
                            child: const Text('Ok'),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          )
                        ],
                      )
              )
                  .then((data) {
                if (resultado?.data?.idFichaClinica != null) {
                  Navigator.of(context).pop();
                }
              });
            }
          },
        ),
      )
    ],
    ),
        ),
    );
  }
}
