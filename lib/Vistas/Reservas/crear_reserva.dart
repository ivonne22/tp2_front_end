import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:tp2_app_medica/Vistas/Reservas/listar_reservas.dart';
import 'package:tp2_app_medica/modelos/api_response.dart';
import 'package:tp2_app_medica/modelos/persona.dart';
import 'package:tp2_app_medica/modelos/reserva.dart';
import 'package:tp2_app_medica/servicios/servicio_personas.dart';
import 'package:tp2_app_medica/servicios/servicio_reservas.dart';

class CrearReserva extends StatefulWidget {
  final Reserva reservaEdit;
  CrearReserva({this.reservaEdit});

  @override
  _CrearReservaState createState() => _CrearReservaState();
}

class _CrearReservaState extends State<CrearReserva> {
  APIResponse<List<Persona>> _apiResponseCliente;
  APIResponse<List<Persona>> _apiResponseEmpleado;
  APIResponse<List<Reserva>> _apiResponseReservas;

  APIResponse<Persona> _apiResponseUnEmpleado;
  APIResponse<Persona> _apiResponseUnCliente;
  List<Persona> _listaEmpleados=[];
  List<Persona> _listaClientes;
  List<Persona> _listaEmpleadosItem;
  List<Persona> _listaClientesItem;
  List<Reserva> _listaReservas;
  List<String> _listaHorariosInicio=[];
  List<String> _listaHorariosFin=[];


  ServicioReserva get servicioReserva => GetIt.I<ServicioReserva>();

  ServicioPersona get servicioPersona => GetIt.I<ServicioPersona>();


  String mensajeError;
  Reserva reserva;

  TextEditingController _controladorFecha = TextEditingController();
  TextEditingController _controladorHoraInicio = TextEditingController();
  TextEditingController _controladorHoraFin = TextEditingController();
  Persona _empleado;
  Persona _cliente;
  TextEditingController _controladorObservacion = TextEditingController();
  String _valorPersona = '';
  DateTime fechaReserva;

  bool _isEnable=false;


  bool _estaCargando = false;
  String _empleadoElegido;
  String _clienteElegido;
  String _horarioInicioElegido;
  String _horarioFinElegido;
  String fechaElegida;

  @override
  void initState() {
    super.initState();
    _fetchEmpleados();
    _fetchClientes();
  }
    _fetchEmpleados() async {
      setState(() {
        _estaCargando = true;
      });

      _apiResponseEmpleado = await servicioPersona.getMedicos();
      _listaEmpleados = _apiResponseEmpleado.data;


      setState(() {
        _estaCargando = false;
      });
    }



    _fetchClientes() async {
      setState(() {
        _estaCargando = true;
      });

      _apiResponseCliente = await servicioPersona.getListaPersonas();
      _listaClientes = _apiResponseCliente.data;

      _listaClientesItem = _listaClientes;
      

      setState(() {
        _estaCargando = false;
      });
    }
    _fetchHorarios() async{
      print(fechaReserva.toString());
      print(_empleadoElegido);
      if(fechaReserva!=null && !_empleadoElegido.isEmpty){
        fechaElegida=fechaReserva.year.toString()+fechaReserva.month.toString()+fechaReserva.day.toString();
        _apiResponseReservas = await servicioReserva.agendaReservaLibres(int.parse(_empleadoElegido), fechaElegida);
        _listaReservas=[];
        _listaHorariosInicio=[];
        _listaHorariosFin=[];
        _listaReservas = _apiResponseReservas.data;
        for(var item in _listaReservas){
          _listaHorariosInicio.add(item.horaInicioCadena);
          _listaHorariosFin.add(item.horaFinCadena);
        }
        _isEnable=true;
      }
      setState(() {
        _estaCargando = true;
      });
      setState(() {
        _estaCargando = false;
      });

    }

  _fetchPersonaEmpleado(int id) async {
    _apiResponseUnEmpleado = await servicioPersona.getPersona(id);
    _empleado = _apiResponseUnEmpleado.data;
    print(_empleado.nombre);
  }

  _fetchPersonaCliente(int id) async {
    _apiResponseUnCliente = await servicioPersona.getPersona(id);
    _cliente = _apiResponseUnCliente.data;
    print(_cliente.nombre);
  }


  @override
    Widget build(BuildContext context) {
      return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(title: Text('Crear reserva')),
        body: Padding(
          padding: const EdgeInsets.all(12.0),
          child: _estaCargando
              ? Center(child: CircularProgressIndicator())
              : Column(
            children: <Widget>[


              Text(fechaReserva==null ? 'Fecha':fechaReserva.toString()),
              TextButton(onPressed: (){
                showDatePicker(context: context,
                    initialDate: fechaReserva ?? DateTime.now(),
                    firstDate: DateTime(1900),
                    lastDate: DateTime(2222)
                ).then((date){
                  setState(() {
                    fechaReserva=DateUtils.dateOnly(date);
                    _fetchHorarios();
                  });
                });

              },
                  child: const Text('Elija una fecha')
              ),

              Container(height: 8),

              ListTile(
                title: const Text('Empleado'),
                trailing:  DropdownButton(
                  items: _listaEmpleados.map((item) {
                    return DropdownMenuItem(
                      child: Text(item.nombre + " " + item.apellido),
                      value: item.idPersona.toString(),
                    );
                  }).toList(),
                  onChanged: (newVal) {
                    setState(() {
                      _empleadoElegido = newVal;
                      _fetchHorarios();
                      _fetchPersonaEmpleado(int.parse(_empleadoElegido));
                    });
                  },
                  value: _empleadoElegido,
                ),
              ),

              Container(height: 8),

              ListTile(
                title: const Text('Hora de inicio'),
                enabled: _isEnable,
                trailing:  DropdownButton(
                  items: _listaHorariosInicio.map((item) {
                    return DropdownMenuItem(
                      child: Text(item),
                      value: item.toString(),
                    );
                  }).toList(),
                  onChanged: (newVal) {
                    print(newVal);
                    setState(() {
                      _horarioInicioElegido = newVal;
                    });
                  },
                  value: _horarioInicioElegido,
                ),
              ),

              Container(height: 8),

              ListTile(
                title: const Text('Hora de fin'),
                enabled: _isEnable,
                trailing:  DropdownButton(
                  items: _listaHorariosFin.map((item) {
                    return DropdownMenuItem(
                      child: Text(item),
                      value: item.toString(),
                    );
                  }).toList(),
                  onChanged: (newVal) {
                    print(newVal);
                    setState(() {
                      _horarioFinElegido = newVal;
                    });
                  },
                  value: _horarioFinElegido,
                ),
              ),


              /*
              Container(height: 8),

              TextField(
                controller: _controladorObservacion,
                decoration: const InputDecoration(
                    hintText: 'Teléfono'
                ),
              ),
              */


              Container(height: 8),

              ListTile(
            title: const Text('Cliente'),
            enabled: _isEnable,
            trailing:  DropdownButton(
                  items: _listaClientes.map((item) {
                    return DropdownMenuItem(
                      child: Text(item.nombre + " " + item.apellido),
                      value: item.idPersona.toString(),
                    );
                  }).toList(),
                  onChanged: (newVal) {
                    setState(() {
                      _clienteElegido = newVal;
                      _fetchPersonaCliente(int.parse(_clienteElegido));
                    });
                  },
                  value: _clienteElegido
                ),
              ),


              Container(height: 8),


              SizedBox(
                width: double.infinity,
                height: 35,
                child: ElevatedButton( //Este en verdad es mi widget de boton que esta wrapeado en sizedbox pero solo para cambiar el tamaño del boton
                    child: const Text(
                        'Guardar', style: TextStyle(color: Colors.white)),
                    style: ElevatedButton.styleFrom(
                      /* primary: Colors.redAccent, // fijar el color de la caja del boton
                 side: BorderSide(width: 3, color: Colors.brown), // configura los lados del boton
                  elevation: 3, // pone que tan elevado va a estar el boton
                  shape: RoundedRectangleBorder( // Para colocar el radio de boton
                    borderRadius: BorderRadius.circular(50)
                  ),
                  padding: EdgeInsets.all(5),
                  alignment: Alignment.bottomCenter, */
                    ),
                    onPressed: () async {
                      setState(() {
                        _estaCargando = true;
                      });
                      print(_empleado.nombre);
                      final reserva = Reserva(
                        fechaCadena: fechaElegida,
                        horaInicioCadena: _horarioInicioElegido,
                        horaFinCadena: _horarioFinElegido,
                        idEmpleado: _empleado,
                        idCliente: _cliente,
                      );
                      print(reserva.toJsonCreate().toString());
                      final resultado = await servicioReserva
                          .crearReserva(reserva);
                      setState(() {
                        _estaCargando = false;
                      });

                      const titulo = 'Creado';
                      final texto = resultado.error ? (resultado.errorMessage ??
                          'Ocurrió un error') : 'La reserva ha sido creada';
                      
                      showDialog(context: context,
                          builder: (_) =>
                              AlertDialog(
                                title: Text(titulo),
                                content: Text(texto),
                                actions: <Widget>[
                                  TextButton(
                                    child: const Text('Ok'),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  )
                                ],
                              )
                      )
                          .then((data) {
                          Navigator.of(context).pop();
                      });
                    }
                ),
              ),
            ],
          ),
        ),
      );
    }
  }
