import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:tp2_app_medica/Vistas/Reservas/crear_reserva.dart';
import 'package:tp2_app_medica/Vistas/Reservas/eliminar_reserva.dart';
import 'package:tp2_app_medica/Vistas/Reservas/modificar_reserva.dart';
import 'package:tp2_app_medica/modelos/reserva.dart';
import 'package:tp2_app_medica/modelos/api_response.dart';
import 'package:tp2_app_medica/servicios/servicio_reservas.dart';

class ListaReservas extends StatefulWidget {
  String usuario;
  ListaReservas({this.usuario});
  @override
  State<ListaReservas> createState() => _ListaReservaState(usuario: usuario);
}

class _ListaReservaState extends State<ListaReservas> {
  String usuario;
  _ListaReservaState({this.usuario});

  ServicioReserva get servicio => GetIt.I<ServicioReserva>();
  APIResponse<List<Reserva>> _apiResponse;
  bool _estaCargando = false;
  bool modificable=false;

  String formatDateTime(DateTime dateTime){
    return '${dateTime.day}/${dateTime.month}/${dateTime.year}';
  }

  @override
  void initState() {
    _fetchReserva();
    super.initState();
  }

  _fetchReserva() async {
    setState(() {
      _estaCargando = true;
    });

    _apiResponse = await servicio.getListaReserva();

    setState(() {
      _estaCargando = false;
    });
  }

  _checkModificable(Reserva reserva){
    print(reserva.observacion);
    print(reserva.flagAsistio);
    print(reserva.flagEstado);
    /*
    if(DateTime.now().compareTo(DateTime.parse(reserva.fecha))==-1 && _apiResponse.data[index].flagAsistio!=null && _apiResponse.data[index].flagEstado=="R"){

    }*/
    if(DateTime.now().compareTo(DateTime.parse(reserva.fecha))==-1 && reserva.flagEstado=="R" && reserva.flagAsistio.isEmpty && reserva.observacion.isEmpty){
      modificable=true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Lista de Reservas')),
      floatingActionButton: FloatingActionButton(

        onPressed: (){
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (builder)=> CrearReserva()))
              .then((_) {
            _fetchReserva();
          });
        },
        child: Icon(Icons.add),
      ),
      body: Builder(
        builder: (context){
          if(_estaCargando) {
            return Center(child: CircularProgressIndicator());
          }

          if(_apiResponse.error) {
            return Center(child: Text(_apiResponse.errorMessage));
          }
          return ListView.separated(
            itemBuilder: (context, index) {
              return Dismissible(
                key: ValueKey(_apiResponse.data[index].idReserva),
                direction: DismissDirection.startToEnd,
                onDismissed: (direction) {

                },
                confirmDismiss: (direction) async {
                  final resultado = await showDialog(
                      context: context,
                      builder: (_) => EliminarReserva()
                  );
                  print(resultado);

                  if(resultado){
                    final eliminarResultado=await servicio.eliminarReserva(_apiResponse.data[index].idReserva);
                    var mensaje;
                    if(eliminarResultado !=null && eliminarResultado.data == true){
                      mensaje='La reserva ha sido eliminada';
                    }else{
                      mensaje = eliminarResultado?.errorMessage ?? 'Ocurrió un error';
                    }

                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(mensaje),
                        duration: new Duration(milliseconds: 1000)
                    ));

                    print(eliminarResultado.data);

                    return eliminarResultado?.data ?? false;

                  }

                  return resultado;
                },
                background: Container(
                  color: Colors.red,
                  padding: const EdgeInsets.only(left: 16),
                  child: const Align(child: Icon(Icons.delete, color: Colors.white), alignment: Alignment.centerLeft),
                ),
                child: ListTile(
                  title: Text(
                    "Nº de reserva: "+_apiResponse.data[index].idReserva.toString()+"        Fecha:"+_apiResponse.data[index].fecha.toString(),
                    style: TextStyle(color: Theme.of(context).primaryColor),
                  ),
                  subtitle: Text('Cliente: ${
                      _apiResponse.data[index].idCliente.nombre ?? ''} ${_apiResponse.data[index].idCliente.apellido ?? ''} \n'
                      'Medico: ${_apiResponse.data[index].idEmpleado.nombre ?? ''} ${_apiResponse.data[index].idEmpleado.apellido ?? ''}'),
                  onTap: () => {
                    _checkModificable(_apiResponse.data[index]),
                    if(modificable==true){
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (_) =>
                          ModificarReserva(
                              reservaEdit: (_apiResponse.data[index]))))
                    },
                    modificable=false
                  },
                ),
              );
            },
            separatorBuilder: (context, index) => Divider(height: 1, color: Colors.green),
            itemCount: _apiResponse.data.length,
          );
        },
      ),
    );
  }
}

