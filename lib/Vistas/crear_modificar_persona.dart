import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:tp2_app_medica/modelos/persona.dart';
import 'package:tp2_app_medica/servicios/servicio_personas.dart';


class CrearModificarPersona extends StatefulWidget {


  final int idPersona;
  CrearModificarPersona({this.idPersona});

  @override
  _CrearModificarPersonaState createState() => _CrearModificarPersonaState();
}

class _CrearModificarPersonaState extends State<CrearModificarPersona> {
  bool get estaEditando => widget.idPersona != null;

  static const tipoPersonaItem = <String>[
    'FISICA',
    'JURIDICA',
  ];
  final List<DropdownMenuItem<String>> _dropDownTipoPersona = tipoPersonaItem
      .map((String valor) => DropdownMenuItem(
    child: Text(valor),
    value: valor,
  ),
  )
      .toList();

  final List<PopupMenuItem<String>> _popUpTipoPersona = tipoPersonaItem
      .map((String valor) => PopupMenuItem<String>(
    child: Text(valor),
    value: valor,
  ),
  )
      .toList();


  ServicioPersona get servicioPersona => GetIt.I<ServicioPersona>();

  String mensajeError;
  Persona persona;

  TextEditingController _controladorNombre = TextEditingController();
  TextEditingController _controladorApellido = TextEditingController();
  TextEditingController _controladorEmail = TextEditingController();
  TextEditingController _controladorTelefono = TextEditingController();
  TextEditingController _controladorRuc = TextEditingController();
  TextEditingController _controladorCedula = TextEditingController();
  String _valorTipoPersona = 'FISICA';
  DateTime _fecha;

  bool _estaCargando = false;

  @override
  void initState() {
    super.initState();


    if (estaEditando){

      setState(() {
        _estaCargando = true;
      });

      servicioPersona.getPersona(widget.idPersona)
          .then((response) {
        setState(() {
          _estaCargando = false;
        });

        persona = response.data;
        _controladorNombre.text = persona.nombre;
        _controladorApellido.text = persona.apellido;
        _controladorEmail.text = persona.email;
        _controladorTelefono.text = persona.telefono;
        _controladorRuc.text = persona.ruc;
        _controladorCedula.text = persona.cedula;
        _valorTipoPersona = persona.tipoPersona;
        _fecha = DateTime.parse(persona.fechaNacimiento);

        if (response.error) {
          mensajeError = response.errorMessage ?? 'Ocurrió un error';
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(title: Text(estaEditando ? 'Editar Persona' : 'Crear Persona')),
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: _estaCargando ? Center(child: CircularProgressIndicator()) : Column(
          children: <Widget>[
            TextField(
              controller: _controladorNombre,
              decoration: const InputDecoration(
                  hintText: 'Nombre'
              ),
            ),

            Container(height: 8),

            TextField(
              controller: _controladorApellido,
              decoration: const InputDecoration(
                  hintText: 'Apellido'
              ),
            ),

            Container(height: 8),

            TextField(
              controller: _controladorEmail,
              decoration: const InputDecoration(
                  hintText: 'Email'
              ),
            ),

            Container(height: 8),

            TextField(
              controller: _controladorTelefono,
              decoration: const InputDecoration(
                  hintText: 'Teléfono'
              ),
            ),

            Container(height: 8),

            TextField(
              controller: _controladorRuc,
              decoration: const InputDecoration(
                  hintText: 'Ruc'
              ),
            ),

            Container(height: 8),

            TextField(
              controller: _controladorCedula,
              decoration: const InputDecoration(
                  hintText: 'Cédula'
              ),
            ),

            Container(height: 8),

            ListTile(
              title: const Text('Tipo de Persona'),
              trailing: DropdownButton<String>(
                value: _valorTipoPersona,
                onChanged: (String nuevoValor){
                  if(nuevoValor != null){
                    setState(() {
                      _valorTipoPersona = nuevoValor;
                    });
                  }
                },
                items: _dropDownTipoPersona,
              ),
            ),

            Container(height: 8),

            Text(_fecha == null ? 'Fecha de nacimiento' : _fecha.toString()),
            TextButton(onPressed: (){
              showDatePicker(context: context,
                  initialDate: _fecha ?? DateTime.now(),
                  firstDate: DateTime(1900),
                  lastDate: DateTime(2222)
              ).then((date) {
                setState(() {
                  _fecha = date;
                });
              });
            },
              child: const Text('Elija una fecha'),
            ),


            Container(height: 16,),

            SizedBox(
              width: double.infinity,
              height: 35,
              child:ElevatedButton( //Este en verdad es mi widget de boton que esta wrapeado en sizedbox pero solo para cambiar el tamaño del boton
                child: const Text('Guardar', style: TextStyle(color: Colors.white)),
                style: ElevatedButton.styleFrom(
                  /* primary: Colors.redAccent, // fijar el color de la caja del boton
                 side: BorderSide(width: 3, color: Colors.brown), // configura los lados del boton
                  elevation: 3, // pone que tan elevado va a estar el boton
                  shape: RoundedRectangleBorder( // Para colocar el radio de boton
                    borderRadius: BorderRadius.circular(50)
                  ),
                  padding: EdgeInsets.all(5),
                  alignment: Alignment.bottomCenter, */
                ),
                onPressed: () async {
                  if (estaEditando) {

                    setState(() {
                      _estaCargando = true;
                    });

                    final persona = Persona(
                      idPersona: widget.idPersona,
                      nombre: _controladorNombre.text,
                      apellido: _controladorApellido.text,
                      email: _controladorEmail.text,
                      telefono: _controladorTelefono.text,
                      ruc: _controladorRuc.text,
                      cedula: _controladorCedula.text,
                      tipoPersona: _valorTipoPersona,
                      fechaNacimiento: _fecha.toString().substring(0,_fecha.toString().indexOf('.')),
                    );
                    final resultado = await servicioPersona.editarPersona(persona);

                    setState(() {
                      _estaCargando = false;
                    });

                    const titulo = 'Actualizado';
                    final texto = resultado.error ? (resultado.errorMessage ??
                        'Ocurrió un error') : 'La persona ha sido actualizada';


                    showDialog(context: context,
                        builder: (_) =>
                            AlertDialog(
                              title: Text(titulo),
                              content: Text(texto),
                              actions: <Widget>[
                                TextButton(
                                  child: const Text('Ok'),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                )
                              ],
                            )
                    )
                        .then((data) {
                      Navigator.of(context).pop();
                    });
                  } else {

                    setState(() {
                      _estaCargando = true;
                    });

                    final persona = Persona(
                      nombre: _controladorNombre.text,
                      apellido: _controladorApellido.text,
                      email: _controladorEmail.text,
                      telefono: _controladorTelefono.text,
                      ruc: _controladorRuc.text,
                      cedula: _controladorCedula.text,
                      tipoPersona: _valorTipoPersona,
                      fechaNacimiento: _fecha.toString().substring(0,_fecha.toString().indexOf('.')),
                    );
                    final resultado = await servicioPersona.crearPersona(
                        persona);

                    setState(() {
                      _estaCargando = false;
                    });

                    const titulo = 'Creado';
                    final texto = resultado.error ? (resultado.errorMessage ??
                        'Ocurrió un error') : 'La persona ha sido creada';


                    showDialog(context: context,
                        builder: (_) =>
                            AlertDialog(
                              title: Text(titulo),
                              content: Text(texto),
                              actions: <Widget>[
                                TextButton(
                                  child: const Text('Ok'),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                )
                              ],
                            )
                    )
                        .then((data) {
                      if (resultado?.data?.idPersona != null) {
                        Navigator.of(context).pop();
                      }
                    });
                  }
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
