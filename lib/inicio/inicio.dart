import 'package:flutter/material.dart';

import 'componentes/body.dart';
import 'componentes/headerinicio.dart';
import 'componentes/widget_drawer.dart';

//Navigator.pushNamed(context, PantallaInicio.routeName)
class PantallaInicio extends StatelessWidget{
  static String routeName = "/home";
  String usuario;
  PantallaInicio({this.usuario});
  @override
  Widget build(BuildContext context){
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Inicio'),
        ),
        drawer: MenuLateral(),
        body: Body(usuario: this.usuario),
      ),
    );
  }
}