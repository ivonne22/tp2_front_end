import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tp2_app_medica/Vistas/Reservas/inicio_reservas.dart';
import 'package:tp2_app_medica/Vistas/Reservas/listar_reservas.dart';
import 'package:tp2_app_medica/Vistas/ficha_clinica/inicio_ficha_clinica.dart';
import 'package:tp2_app_medica/Vistas/ficha_clinica/listar_ficha_clinica.dart';
import 'package:tp2_app_medica/Vistas/inicio_personas.dart';

import '../../size_config.dart';
import 'headerinicio.dart';

class Body extends StatelessWidget {
  static MediaQueryData _mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double defaultSize;
  static Orientation orientation;

  String usuario;
  Body({this.usuario});



  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Flexible(
                    child: Image.asset('images/logo.png', height: 340.0),
                  ),
                  SizedBox(height: 30,),
                  _Pacientes(),
                  SizedBox(height: 30,),
                  _Reservas(),
                  SizedBox(height: 30,),
                  _FichasClinicas(),

                ]
            )
        ),
      ),
    );
  }

  Widget _Pacientes() {
    return StreamBuilder(
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return ElevatedButton(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 89, vertical: 20),
                child: Text('Pacientes',
                    style: TextStyle(
                        fontSize: 20)),
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => InicioPersonas()));
              });
        }
    );
  }

  Widget _Reservas() {
    return StreamBuilder(
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return ElevatedButton(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 89, vertical: 20),
                child: Text('Reservas',
                    style: TextStyle(
                        fontSize: 20)),
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => InicioReservas()));
              });
        }
    );
  }

  Widget _FichasClinicas() {
    return StreamBuilder(
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return ElevatedButton(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 68, vertical: 20),
                child: Text('Fichas Clínicas',
                    style: TextStyle(
                        fontSize: 20)),
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => InicioFichaClinica()));
              });
        }
    );
  }
}