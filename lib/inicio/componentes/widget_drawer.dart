import 'package:flutter/material.dart';
import 'package:tp2_app_medica/Vistas/Reservas/inicio_reservas.dart';
import 'package:tp2_app_medica/Vistas/ficha_clinica/inicio_ficha_clinica.dart';
import 'package:tp2_app_medica/Vistas/inicio_personas.dart';

class MenuLateral extends StatelessWidget {
  String usuario;
  MenuLateral({this.usuario});
  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            UserAccountsDrawerHeader(
              accountName: Text('User tester'),
              accountEmail: Text('YourEmail@email.com'),
              currentAccountPicture: CircleAvatar(
                backgroundColor: Colors.white,
                  child: Text(
                      'U',style: TextStyle(fontSize: 40),
                  ),
              ),
            ),
            ListTile(
              leading: Icon(Icons.groups),
              title: Text('Pacientes'),
              onTap: (){
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return InicioPersonas();
                      },
                    ),
                );
              },
            ),
            Divider(color: Colors.white),
            ListTile(
              leading: Icon(Icons.calendar_today),
              title: Text('Reservas'),
              onTap: (){
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => InicioReservas(usuario: this.usuario)));
              },
            ),
            Divider(color: Colors.white),
            ListTile(
              leading: Icon(Icons.auto_stories),
              title: Text('Fichas clínicas'),
              onTap: (){
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => InicioFichaClinica(usuario: this.usuario)));
              },
            ),
            Divider(color: Colors.white),
          ],
        )
    );
  }
}